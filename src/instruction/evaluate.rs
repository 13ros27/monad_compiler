use std::collections::HashSet;

use super::hashset::{HashSetAdd, HashSetDiv, HashSetMod, HashSetMul};
use super::Instruction;
use crate::program::Program;
use crate::value::{Value, Vid};

impl Instruction {
    pub fn evaluate(&self, program: &mut Program, left: Vid, right: Vid) -> Vid {
        match self {
            Instruction::Input(_) => unreachable!(),
            Instruction::Add(..) => self.evaluate_add(program, left, right),
            Instruction::Mul(..) => self.evaluate_mul(program, left, right),
            Instruction::Div(..) => self.evaluate_div(program, left, right),
            Instruction::Mod(..) => self.evaluate_mod(program, left, right),
            Instruction::Equal(..) => self.evaluate_equal(program, left, right),
        }
    }

    fn evaluate_add(&self, program: &mut Program, left: Vid, right: Vid) -> Vid {
        let left_val = program.get_value(&left);
        let right_val = program.get_value(&right);
        match (left_val, right_val) {
            (Value::Exact(l), Value::Exact(r)) => {
                // Getting around mutable_borrow_reservation_conflict
                let part = l + r;
                program.new_exact(part)
            }
            (_, Value::Exact(0)) => left,  // ? + 0 = ?
            (Value::Exact(0), _) => right, // 0 + ? = ?
            (Value::Constraint(con), Value::Exact(ex)) => {
                let new_constraint = con.add(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Exact(ex), Value::Constraint(con)) => {
                let new_constraint = con.add(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Constraint(l), Value::Constraint(r)) => {
                let new_constraint = l.add(r);
                program.new_constraint(new_constraint)
            }
            _ => program.new_unknown(),
        }
    }

    fn evaluate_mul(&self, program: &mut Program, left: Vid, right: Vid) -> Vid {
        let left_val = program.get_value(&left);
        let right_val = program.get_value(&right);
        match (left_val, right_val) {
            (Value::Exact(l), Value::Exact(r)) => {
                let part = l * r;
                program.new_exact(part)
            }
            (_, Value::Exact(0)) => right, // ? * 0 = 0
            (Value::Exact(0), _) => left,  // 0 * ? = 0
            (_, Value::Exact(1)) => left,  // ? * 1 = ?
            (Value::Exact(1), _) => right, // 1 * ? = ?
            (Value::Constraint(con), Value::Exact(ex)) => {
                let new_constraint = con.mul(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Exact(ex), Value::Constraint(con)) => {
                let new_constraint = con.mul(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Constraint(l), Value::Constraint(r)) => {
                let new_constraint = l.mul(r);
                program.new_constraint(new_constraint)
            }
            _ => program.new_unknown(),
        }
    }

    fn evaluate_div(&self, program: &mut Program, left: Vid, right: Vid) -> Vid {
        let left_val = program.get_value(&left);
        let right_val = program.get_value(&right);
        match (left_val, right_val) {
            (Value::Exact(l), Value::Exact(r)) => {
                let part = l / r;
                program.new_exact(part)
            }
            (Value::Exact(0), _) => left, // 0 / ? = 0
            (_, Value::Exact(1)) => left, // ? / 1 = ?
            (Value::Constraint(con), Value::Exact(ex)) => {
                let new_constraint = con.div(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Exact(ex), Value::Constraint(con)) => {
                let new_constraint = con.div(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Constraint(l), Value::Constraint(r)) => {
                let new_constraint = l.div(r);
                program.new_constraint(new_constraint)
            }
            _ => program.new_unknown(),
        }
    }

    fn evaluate_mod(&self, program: &mut Program, left: Vid, right: Vid) -> Vid {
        let left_val = program.get_value(&left);
        let right_val = program.get_value(&right);
        match (left_val, right_val) {
            (Value::Exact(l), Value::Exact(r)) => {
                let part = l % r;
                program.new_exact(part)
            }
            (Value::Exact(0), _) => left,                 // 0 % ? = 0
            (_, Value::Exact(1)) => program.new_exact(0), // ? % 1 = 0
            (Value::Constraint(con), Value::Exact(ex)) => {
                let new_constraint = con.modu(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Exact(ex), Value::Constraint(con)) => {
                let new_constraint = con.modu(ex);
                program.new_constraint(new_constraint)
            }
            (Value::Constraint(l), Value::Constraint(r)) => {
                let new_constraint = l.modu(r);
                program.new_constraint(new_constraint)
            }
            _ => program.new_unknown(),
        }
    }

    fn evaluate_equal(&self, program: &mut Program, left: Vid, right: Vid) -> Vid {
        if left == right {
            return program.new_exact(1);
        }
        let left_val = program.get_value(&left);
        let right_val = program.get_value(&right);
        match (left_val, right_val) {
            (Value::Exact(l), Value::Exact(r)) if l != r => program.new_exact(0),
            (Value::Constraint(con), Value::Exact(ex)) if !con.contains(ex) => program.new_exact(0),
            (Value::Exact(ex), Value::Constraint(con)) if !con.contains(ex) => program.new_exact(0),
            _ => program.new_constraint(HashSet::from_iter(vec![0, 1])),
        }
    }
}
