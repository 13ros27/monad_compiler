mod evaluate;
mod hashset;

use std::fmt::{Display, Formatter};
use std::ops::Deref;

/// A register in a MONAD instruction.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Register(pub usize);

impl Register {
    pub fn new(name: &str) -> Register {
        match name {
            "w" => Register(0),
            "x" => Register(1),
            "y" => Register(2),
            "z" => Register(3),
            _ => panic!("Attempting to create a register of {}", name),
        }
    }
}

impl Deref for Register {
    type Target = usize;
    fn deref(&self) -> &usize {
        &self.0
    }
}

impl Display for Register {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{}",
            match self.0 {
                0 => "w",
                1 => "x",
                2 => "y",
                3 => "z",
                _ => unreachable!(),
            }
        )
    }
}

/// The second operand of a MONAD instruction.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Operand {
    Literal(i64),
    Register(Register),
}

impl Display for Operand {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        match self {
            Operand::Literal(num) => write!(f, "{num}"),
            Operand::Register(reg) => write!(f, "{reg}"),
        }
    }
}

/// An instruction in the MONAD language.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Instruction {
    Input(Register),          // e.g. inp x
    Add(Register, Operand),   // e.g. add x 2
    Mul(Register, Operand),   // e.g. mul x 0
    Div(Register, Operand),   // e.g. div x 10
    Mod(Register, Operand),   // e.g. mod x 31
    Equal(Register, Operand), // e.g. eql x y
}

impl Instruction {
    pub fn register(&self) -> Register {
        match self {
            Self::Input(r) => *r,
            Self::Add(r, _) => *r,
            Self::Mul(r, _) => *r,
            Self::Div(r, _) => *r,
            Self::Mod(r, _) => *r,
            Self::Equal(r, _) => *r,
        }
    }

    pub fn operand(&self) -> Option<Operand> {
        match self {
            Self::Input(_) => None,
            Self::Add(_, o) => Some(*o),
            Self::Mul(_, o) => Some(*o),
            Self::Div(_, o) => Some(*o),
            Self::Mod(_, o) => Some(*o),
            Self::Equal(_, o) => Some(*o),
        }
    }
}

impl Display for Instruction {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        match self {
            Self::Input(r) => write!(f, "inp {r}"),
            Self::Add(r, o) => write!(f, "add {r} {o}"),
            Self::Mul(r, o) => write!(f, "mul {r} {o}"),
            Self::Div(r, o) => write!(f, "div {r} {o}"),
            Self::Mod(r, o) => write!(f, "mod {r} {o}"),
            Self::Equal(r, o) => write!(f, "eql {r} {o}"),
        }
    }
}
