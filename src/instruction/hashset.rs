use std::collections::HashSet;

pub trait HashSetAdd<Rhs> {
    fn add(&self, other: &Rhs) -> Self;
}

impl HashSetAdd<i64> for HashSet<i64> {
    fn add(&self, other: &i64) -> HashSet<i64> {
        self.iter().map(|n| n + other).collect()
    }
}

impl HashSetAdd<HashSet<i64>> for HashSet<i64> {
    fn add(&self, other: &HashSet<i64>) -> HashSet<i64> {
        self.iter()
            .flat_map(|n1| other.iter().map(|n2| n1 + n2).collect::<Vec<i64>>())
            .collect()
    }
}

pub trait HashSetMul<Rhs> {
    fn mul(&self, other: &Rhs) -> Self;
}

impl HashSetMul<i64> for HashSet<i64> {
    fn mul(&self, other: &i64) -> HashSet<i64> {
        self.iter().map(|n| n * other).collect()
    }
}

impl HashSetMul<HashSet<i64>> for HashSet<i64> {
    fn mul(&self, other: &HashSet<i64>) -> HashSet<i64> {
        self.iter()
            .flat_map(|n1| other.iter().map(|n2| n1 * n2).collect::<Vec<i64>>())
            .collect()
    }
}

pub trait HashSetDiv<Rhs> {
    fn div(&self, other: &Rhs) -> Self;
}

impl HashSetDiv<i64> for HashSet<i64> {
    fn div(&self, other: &i64) -> HashSet<i64> {
        self.iter().map(|n| n / other).collect()
    }
}

impl HashSetDiv<HashSet<i64>> for HashSet<i64> {
    fn div(&self, other: &HashSet<i64>) -> HashSet<i64> {
        self.iter()
            .flat_map(|n1| other.iter().map(|n2| n1 / n2).collect::<Vec<i64>>())
            .collect()
    }
}

pub trait HashSetMod<Rhs> {
    fn modu(&self, other: &Rhs) -> Self;
}

impl HashSetMod<i64> for HashSet<i64> {
    fn modu(&self, other: &i64) -> HashSet<i64> {
        self.iter().map(|n| n % other).collect()
    }
}

impl HashSetMod<HashSet<i64>> for HashSet<i64> {
    fn modu(&self, other: &HashSet<i64>) -> HashSet<i64> {
        self.iter()
            .flat_map(|n1| other.iter().map(|n2| n1 % n2).collect::<Vec<i64>>())
            .collect()
    }
}
