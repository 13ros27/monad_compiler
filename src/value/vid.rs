use std::fmt::{Display, Formatter};

use crate::unique_id::UniqueIdMaker;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Vid(pub usize);

impl Vid {
    pub fn unique_id_maker() -> UniqueIdMaker<Vid> {
        UniqueIdMaker::starting_at(0)
    }
}

impl From<usize> for Vid {
    fn from(x: usize) -> Self {
        Self(x)
    }
}

impl Display for Vid {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.0)
    }
}
