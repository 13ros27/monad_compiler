mod vid;

use std::collections::HashSet;
use std::fmt::{Display, Formatter};

pub use vid::Vid;

#[derive(Clone, Debug, Eq)]
pub enum Value {
    Exact(i64),
    Constraint(HashSet<i64>),
    Input(usize),
    Unknown,
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        match self {
            Value::Exact(exact) => write!(f, "Exact({exact})"),
            Value::Constraint(constraint) => write!(f, "Constraint({:?})", constraint),
            Value::Input(input) => write!(f, "Input({input})"),
            Value::Unknown => write!(f, "Unknown"),
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Value) -> bool {
        if let (&Self::Exact(left), &Self::Exact(right)) = (self, other) {
            left == right
        } else {
            false
        }
    }
}
