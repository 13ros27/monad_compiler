use crate::instruction::{Instruction, Operand, Register};

pub fn lexer(text: &str) -> Vec<Instruction> {
    let mut instructions = Vec::new();
    for line in text.lines() {
        let words: Vec<&str> = line.split(' ').collect();
        if words[0] == "inp" {
            instructions.push(Instruction::Input(Register::new(words[1])))
        } else {
            let reg = Register::new(words[1]);
            let first_char = words[2].chars().next().unwrap();
            let op = if first_char.is_numeric() | (first_char == '-') {
                Operand::Literal(words[2].parse().unwrap())
            } else {
                Operand::Register(Register::new(words[2]))
            };
            instructions.push(match words[0] {
                "add" => Instruction::Add(reg, op),
                "mul" => Instruction::Mul(reg, op),
                "div" => Instruction::Div(reg, op),
                "mod" => Instruction::Mod(reg, op),
                "eql" => Instruction::Equal(reg, op),
                _ => unreachable!(),
            });
        }
    }
    instructions
}
