use std::collections::{HashMap, HashSet};

use crate::unique_id::UniqueIdMaker;
use crate::value::{Value, Vid};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Program {
    vid_maker: UniqueIdMaker<Vid>,
    initial_registers: [Vid; 4],
    next_input_id: usize,
    vid_value_linker: HashMap<Vid, Value>,
}

impl Program {
    pub fn new() -> Program {
        let mut vid_maker = Vid::unique_id_maker();
        let initial_registers = [
            vid_maker.make_new_id(),
            vid_maker.make_new_id(),
            vid_maker.make_new_id(),
            vid_maker.make_new_id(),
        ];
        let mut vid_value_linker = HashMap::new();
        initial_registers.iter().for_each(|vid| {
            vid_value_linker.insert(*vid, Value::Exact(0));
        });
        Program {
            vid_maker,
            initial_registers,
            next_input_id: 0,
            vid_value_linker,
        }
    }

    pub fn initial_registers(&self) -> [Vid; 4] {
        self.initial_registers
    }

    pub fn new_vid(&mut self, value: Value) -> Vid {
        let vid = self.vid_maker.make_new_id();
        self.vid_value_linker.insert(vid, value);
        vid
    }

    pub fn new_exact(&mut self, exact_val: i64) -> Vid {
        self.new_vid(Value::Exact(exact_val))
    }

    pub fn new_constraint(&mut self, values: HashSet<i64>) -> Vid {
        self.new_vid(Value::Constraint(values))
    }

    pub fn new_unknown(&mut self) -> Vid {
        self.new_vid(Value::Unknown)
    }

    pub fn new_input(&mut self) -> Vid {
        let next_input_id = self.next_input_id;
        self.next_input_id += 1;
        self.new_vid(Value::Input(next_input_id))
    }

    pub fn get_value(&self, vid: &Vid) -> &Value {
        self.vid_value_linker.get(vid).unwrap() // Shouldn't be possible to have a Vid without an associated Value
    }
}
