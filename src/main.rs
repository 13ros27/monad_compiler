#![allow(clippy::format_in_format_args)] // This lint doesn't work
use std::fs::File;
use std::io::Read;

mod compiler;
mod instruction;
mod lexer;
mod program;
mod unique_id;
mod value;

fn main() -> std::io::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    if args.len() <= 1 {
        panic!("Missing one required argument");
    }
    let (filepath, registers) = if args.len() == 2 {
        (&args[1], false)
    } else {
        (&args[2], true)
    };
    let mut file = File::open(filepath)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let instructions = lexer::lexer(&contents);
    let instr_len = instructions.len();
    let compiler = compiler::Compiler::compile(instructions.into());
    if registers {
        println!("{}", compiler);
    } else {
        println!("{}", instr_len);
        println!("{}", compiler.instrs.instruction_count());
    }
    Ok(())
}
