use std::fmt::{Display, Formatter};
use std::ops::{Deref, DerefMut, Index};

use crate::instruction::{Instruction, Operand, Register};
use crate::program::Program;
use crate::value::{Value, Vid};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Instructions {
    instrs: Vec<Instruction>,
    pub nops: Vec<bool>,
}

impl Instructions {
    pub fn instruction_count(&self) -> usize {
        self.nops.iter().filter(|&b| !*b).count()
    }
}

impl Deref for Instructions {
    type Target = Vec<Instruction>;
    fn deref(&self) -> &Vec<Instruction> {
        &self.instrs
    }
}

impl From<Vec<Instruction>> for Instructions {
    fn from(instrs: Vec<Instruction>) -> Instructions {
        let len = instrs.len();
        Instructions {
            instrs,
            nops: vec![false; len],
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Registers(Vec<[Vid; 4]>);

impl Registers {
    fn add_row(&mut self, reg: &Register, vid: Vid) {
        let mut new_row = *self.0.last().unwrap();
        new_row[**reg] = vid;
        self.0.push(new_row);
    }
}

impl Deref for Registers {
    type Target = Vec<[Vid; 4]>;
    fn deref(&self) -> &Vec<[Vid; 4]> {
        &self.0
    }
}

impl From<[Vid; 4]> for Registers {
    fn from(registers: [Vid; 4]) -> Registers {
        Registers(vec![registers])
    }
}

impl Index<usize> for Registers {
    type Output = Vid;
    fn index(&self, index: usize) -> &Vid {
        &self.0.last().unwrap()[index]
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct SubDeadCode([Vec<usize>; 4]);

impl Deref for SubDeadCode {
    type Target = [Vec<usize>; 4];
    fn deref(&self) -> &[Vec<usize>; 4] {
        &self.0
    }
}

impl DerefMut for SubDeadCode {
    fn deref_mut(&mut self) -> &mut [Vec<usize>; 4] {
        &mut self.0
    }
}

impl SubDeadCode {
    fn find(&self, line: usize) -> Option<usize> {
        for (i, reg) in self.0.iter().enumerate() {
            if reg.contains(&line) {
                return Some(i);
            }
        }
        None
    }

    fn remove_up_to(&mut self, register: usize, line: usize) {
        for _ in 0..self.0[register].len() {
            if self.0[register][0] != line {
                self.0[register].remove(0);
            }
        }
        self.0[register].remove(0);
    }

    fn remove(&mut self, lines: Vec<usize>) {
        for line in &lines {
            let mut remove = Vec::new();
            for (i, reg) in self.0.iter().enumerate() {
                if let Some(loc) = reg.iter().position(|l| l == line) {
                    remove.push((i, loc));
                }
            }
            remove.iter().for_each(|(i, l)| {self.0[*i].remove(*l);});
        }
    }

    fn nop_down_to(&mut self, nops: &mut Vec<bool>, register: usize, line: usize) -> Vec<usize> {
        let mut removed = Vec::new();
        for i in (0..self.0[register].len()).rev() {
            let current = *self.0[register].last().unwrap();
            if current <= line {
                break;
            }
            removed.push(current - i);
            nops[current - i] = true;
        }
        removed
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DeadCode {
    modify: SubDeadCode,
    used_by: SubDeadCode,
}

impl DeadCode {
    fn fill(&mut self, instr: &Instruction, index: usize) {
        self.modify[*instr.register()].push(index);
        if let Operand::Register(Register(right)) = instr.operand().unwrap() {
            self.used_by[right].push(index)
        }
    }

    fn clear(&mut self, register: usize) {
        self.modify[register] = Vec::new();
        for &line in &self.used_by[register] {
            if let Some(mod_reg) = self.modify.find(line) {
                self.modify.remove_up_to(mod_reg, line);
            }
        }
    }

    fn wipe(&mut self, nops: &mut Vec<bool>, register: usize) -> Option<usize> {
        let line = if let Some(last_val) = self.used_by[register].last() {
            *last_val
        } else {
            0
        };
        if line != 0 {
            self.used_by[register].pop();
        }
        let removed = self.modify.nop_down_to(nops, register, line);
        self.modify[register] = Vec::new();
        if !removed.is_empty() {
            let original_state = removed.last().unwrap() - 1;
            self.used_by.remove(removed);
            Some(original_state)
        } else {
            None
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Compiler {
    pub instrs: Instructions,
    registers: Registers,
    program: Program,
    pub dead_code: DeadCode,
}

impl Compiler {
    pub fn compile(instrs: Instructions) -> Self {
        let program = Program::new();
        let registers: Registers = program.initial_registers().into();
        let mut compiler = Compiler {
            instrs,
            registers,
            program,
            dead_code: DeadCode::default(),
        };
        compiler.constant_propagation();
        compiler
    }

    fn constant_propagation(&mut self) {
        for (i, instr) in self.instrs.instrs.iter().enumerate() {
            if let Instruction::Input(reg) = instr {
                self.registers.add_row(reg, self.program.new_input());
                // CLEAR
                self.dead_code.clear(**reg);
            } else {
                let destination_register = instr.register();
                let left = self.registers[*destination_register];
                let right = match instr.operand().unwrap() {
                    Operand::Literal(lit) => self.program.new_exact(lit),
                    Operand::Register(Register(r)) => self.registers[r],
                };

                let mut previous_register_value = self.registers[*destination_register];

                let new_register_value = instr.evaluate(&mut self.program, left, right);
                self.registers
                    .add_row(&destination_register, new_register_value);

                // WIPE
                if let Instruction::Mul(l, r) = self.instrs[i] {
                    let wipe = match r {
                        Operand::Literal(0) => true,
                        Operand::Register(reg) if *self.program.get_value(&self.registers[*reg]) == Value::Exact(0) => true,
                        _ => false
                    };
                    if wipe {
                        if let Some(wiped_to) = self.dead_code.wipe(&mut self.instrs.nops, *l) {
                            previous_register_value = self.registers.0[wiped_to + 1][*destination_register]
                        }
                    }
                }

                if previous_register_value == new_register_value
                    || self.program.get_value(&previous_register_value)
                        == self.program.get_value(&new_register_value)
                {
                    self.instrs.nops[i] = true;
                } else {
                    // MODIFY
                    self.dead_code.fill(instr, i);
                }
            }
        }
    }
}

impl Display for Compiler {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        let mut text = String::new();
        for (i, register) in self.registers.iter().enumerate() {
            let (instr, nop) = if i == 0 {
                ("<start>".to_string(), false)
            } else {
                (format!("{}", self.instrs[i - 1]), self.instrs.nops[i - 1])
            };
            text.push_str(&format!(
                "{:^9} [ {:^25} | {:^25} | {:^25} | {:^25} ]{}\n",
                instr,
                format!("{}: {}", register[0], self.program.get_value(&register[0])),
                format!("{}: {}", register[1], self.program.get_value(&register[1])),
                format!("{}: {}", register[2], self.program.get_value(&register[2])),
                format!("{}: {}", register[3], self.program.get_value(&register[3])),
                {
                    if nop {
                        " NOP"
                    } else {
                        ""
                    }
                }
            ))
        }
        text.pop();
        write!(f, "{}", text)
    }
}
